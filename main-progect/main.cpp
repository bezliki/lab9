#include <iostream>
#include <algorithm>
#include "filereader.h"
#include "marafon.h"
#include "mydata.h"
#include "mywork.h"

using namespace std;
int main()
{
    setlocale(LC_ALL, "rus");
    cout << "������������ ������ #9. GIT\n";
    cout << "������� #1. ���������� ��������\n";
    cout << "�����: ����� ���-���-��\n";
    cout << "������: 11\n";

    Data data[5];
    for (int i = 0; i < 5; i++)
        data[i] = insertData();
    int min;

    cout << "��������: 0-��� �� ��������, 1 - ���, � ���� ����� ����� ��� 2:50:00\n";
    cin >> min;

    if (min == 0)
        showSpartak(data, 5);
    else if (min == 1)
        compareData(data, 5);
    int sort = 0, compType = 0;

    cout << "��������: 0 - ���������� ���������, 1 - ������� ����������\n";
    cin >> sort;
    cout << "��������: 0 - �����, 1 - Name > Second_Name\n";
    cin >> compType;
    //8/ std::sort(data, data + 5, comps[compType]);
    if (sort == 1)
        if (compType == 1)
            std::sort(data, data + 5, compClub);
        else
            std::sort(data, data + 5, compByTime);
    else
        if (compType == 1)
            bubbleSort(data, 0, 5, compClub);
        else
            bubbleSort(data, 0, 5, compByTime);

    for (int i = 0; i < 5; i++)
        mydata(data[i]);
    return 0;
}